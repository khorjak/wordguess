var app = new Vue({
    el: '#app',
    data: {
        isGameRunning: false,
        numOfTries: 0,
        guessLetter: '',
        guessWord: [],
        wordToGuess: '',
        finishMessage: ''
    },
    methods: {
        startGame: function() {
            this.isGameRunning = true;
            this.numOfTries = 0;
            this.finishMessage = '';
            this.newWord();
        },
        newWord: function() {
            this.randomWord();
        },
        tryLetter: function() {
            var letter = this.guessLetter;
            var isCorrect = false;
            for (var index = 0; index < this.wordToGuess.length; index++) {
                var element = this.wordToGuess[index].toUpperCase();
                if (letter.toUpperCase() == element) {
                    this.guessWord[index] = element;
                    isCorrect = true;
                }
            }
            if (!isCorrect) {
                this.numOfTries++;
            }            
            this.checkWinLost();
            this.guessLetter = '';
        },
        checkWinLost: function() {
            if (this.numOfTries >= 10) {
                this.finishMessage = 'You lose! The word is ' + this.wordToGuess;
                this.isGameRunning = false;
            }
            if (this.guessWord.indexOf(' ') == -1) {
                this.finishMessage = 'You win!';
                this.isGameRunning = false;
            }
        },
        randomWord: function() {
            var requestUrl = "http://www.setgetgo.com/randomword/get.php";
            this.$http.get(requestUrl).then((response) => {
                // success callback
                this.wordToGuess = response.body;
                this.guessWord = [];
                for (var index = 0; index < this.wordToGuess.length; index++) {
                    this.guessWord[index] = ' ';
                }
            }, (response) => {
                // error callback
                this.wordToGuess = '';
            });
        }
    }
});
